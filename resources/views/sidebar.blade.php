<!-- Left side column. contains the sidebar -->
<aside class="main-sidebar">

    <!-- sidebar: style can be found in sidebar.less -->
    <section class="sidebar">

        <!-- Sidebar Menu -->
         <ul class="sidebar-menu">
            <li class="header"><i class="fa fa-tachometer" aria-hidden="true"></i>MENU</li>
            <!-- Optionally, you can add icons to the links -->
            <!-- <li class="active"><a href="#"><span>Link</span></a></li> -->
            <li><a href="{{URL::to('admin/home')}}"><i class="fa fa-home" aria-hidden="true"></i><span>Dashboard</span></a></li>
            <li><a href="{{URL::to('admin/users')}}"><i class="fa fa-users" aria-hidden="true"></i><span>Users</span></a></li>
            <li class="treeview">
                <a href="{{URL::to('admin/network')}}"><i class="fa fa-signal"></i><span>Network</span></a>
                <ul class="treeview-menu">
                    <li><a href="{{URL::to('admin/network/mtn')}}">MTN</a></li>
                    <li><a href="{{URL::to('admin/network/vivacell')}}">Vivacell</a></li>
                </ul>
            </li> 
             <li><a href="{{URL::to('admin/users')}}"><i class="fa fa-tablet" aria-hidden="true"></i><span>Terminals</span></a></li>
            <li><a href="{{URL::to('admin/transactions')}}"><i class="fa fa-bar-chart" aria-hidden="true"></i><span>Transaction Totals</span></a></li>
            <li><a href="{{URL::to('admin/software')}}"><i class="fa fa-download" aria-hidden="true"></i><span>Sofware Issues</span></a></li>
            <li class="treeview">
                <a href="#"><i class="fa fa-exclamation-triangle"></i><span>Hardware Errors!</span></a>
                <ul class="treeview-menu">
                    <li><a href="{{URL::to('admin/software')}}">Battery</a></li>
                    <li><a href="{{URL::to('admin/software')}}">Network</a></li>
                </ul>
            </li>

<!--             <li class="treeview">
                <a href="#"><span>Multilevel</span> <i class="fa fa-exclamation-triangle-left pull-right"></i></a>
                <ul class="treeview-menu">
                    <li><a href="#">Link in level 2</a></li>
                    <li><a href="#">Link in level 2</a></li>
                </ul>
            </li> -->
        </ul><!-- /.sidebar-menu -->
    </section>
    <!-- /.sidebar -->
</aside>
