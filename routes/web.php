<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// Route::get('/', function () {
//     return view('welcome');
// });

Route::get('signin', 'LoginController@index');
Route::post('signin','LoginController@signin');
Route::get('signout','LoginController@signout');

// Route::get('/getTerminals', function(){
// 	$item = ['status'=>'success','data' =>  20];
//     $response = json_encode($item);
//     return $response;
// });



Route::group(['prefix' => 'admin'], function() {
    Route::get('users', ['as' => 'admin.getUsers', 'uses' => 'UsersController@index']);

    // Route::group(['middleware' => ['auth.admin:admin']], function() {
    //     Route::get('/', ['as' => 'admin.home', 'uses' => 'Admin\IndexController@getIndex']);
    // });
});

