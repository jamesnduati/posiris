<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use Illuminate\Support\Facades\Auth;
use Validator;
use Redirect;
//models
use App\User;

class LoginController extends Controller
{
	 /**
     * Show the form for creating a new resource.
     *
     * @return login page
     */
    public function index()
    {
        return view('login');
    }


    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function signin(Request $request)
    {
        $data = $request->all();
        $rules = array(
            'username' => 'filled', 
            'password' => 'filled' 
        );
        $validator = Validator::make($data, $rules);
        if ($validator->fails()) {
            return Redirect::to('signin')
                            ->withInput($request->all())
                            ->with($errors);
        }else{

            if (Auth::attempt($data)) { 

                return view('admin_template');

            }else{
                return Redirect::to('signin');
            }
        }

    }

     /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */

    public function signout()
    {
        Auth::logout();
        return Redirect::to('signin');
    }
}
